# The PolicyFly Center For Children Who Can't Read Good

Insurance is nice, but the _real_ money is in teaching children how to identify words that are difficult to sound out. You ever try to explain to a three-year-old why the _ork_ in _work_ goes "irk" but the _ork_ in _fork_ goes "orc?" Or why every C in "Pacific Ocean" is pronounced differently? It sucks. Parents don't want to do it, and they'll give us money to do it for them.

Strap in. We're gonna get rich teaching kids [sight words](https://en.wikipedia.org/wiki/Sight_word).

This repository hosts the backend API that will power our teaching service. It is organized as a single Django app, `api`, which implements three models:

1. `User` - Your standard custom user auth model
2. `Wordlist` - A collection of sight words that we will use to test our reader. There are two interesting attributes here:
    - `name` - The unique name of the collection, like "Dolch Sight Words Level 1" or "Billy's Problem Words"
    - `words` - An array of words that we will use to test our reader
3. `Lesson` - a record of a user's attempt to identify the words in a `Wordlist`. `Lesson` objects have one interesting attribute: `data`. 

Here's a truncated example of a `Lesson.data` attribute:

```python3
{
  'can': {'attempts': 17, 'nailedIt': True},
  'come': {'attempts': 9, 'nailedIt': True},
  'down': {'attempts': 19, 'nailedIt': False},
  'find': {'attempts': 19, 'nailedIt': False}
}
```

This tells us that our student was tested on four words: `can`, `come`, `down`, and `find`. The `attempts` count tells us how many times the student was shown the sight word. When the student successfully reads the sight word, the `nailedIt` boolean is flipped to True. 

I this example we know that the student read the word `can` successfully after being shown the word 17 times, read the word `come` successfully after being shown the word 9 times, and failed to read either `down` or `find` after 19 tries. There's no maximum number of tries, and it's not required that all words in a list are attempted.

You can run the `seed_test_lessons` management command to generate five random `Lessons` for each `Wordlist` in the database.

## Administering Sight Words Tests

Not our problem. The frontend UI looks like this:

![amazing wireframe](api/assets/professional_wireframe.png)

This incredibly complex Vue.JS application loads a selected `Wordlist` from the relevant detail view. It shuffles the `words` in that list and displays them one at a time in the big blue square, incrementing the `attempts` counter for the word on each display. If the student successfully reads the word, the tester presses the green button and the `nailedIt` boolean for that word is flipped to `True`. That word is then "removed" from the "deck" for the remainder of the test. If the student fails to read the word, the tester presses the red button and the word is shuffled back into the "deck." The tester is encouraged to continue until the student has identified all words successfully, but they can choose to end at any time.

When the tester ends the test, a fully-formed `Lesson.data` packet is sent to the relevant create endpoint. tl;dr - the FE is administering tests, we're just recording the results they send us.

## TODO

We want to begin indexing our `Lessons` in ElasticSearch. Looking in our `docker-compose.yml` file, you'll notice that `elasticsearch` and `kibana` services have been defined. They are not, however, doing anything - yet! That's where you come in.

Using the `django-elasticsearch-dsl` and `django-elasticsearch-dsl-drf` packages, we want to accomplish the following things:

1. Create an index for the `Lesson` model
2. Correctly populate that index on CRUD operations against any given `Lesson` object
3. Expose an API endpoint that allows users to search / filter `Lesson` documents in ES
    - Example searches:
        - All lessons associated with a `wordlist` with a given name
        - All lessons that contain a given word
        - All lessons where a given word was failed / passed
    - Example filters:
        - All lessons where at least X words had at least 1 attempt
        - All lessons before/after a certain date
        
We're not looking to accomplish all of this right now (although if you knock it all out, we're not going to turn it down). Instead, we want you to spend about four hours or so on this. When you hit a good stopping point at around that level of time invested, take an additional few minutes to write down your thoughts:

- Did you run into any problems?
- Did you have to make any assumptions?
- If you were asked to continue working on this, what would be your next steps?
- How would you deliver information about this new endpoint & its functionality to the frontend team?

That's it. Fork this repo and have fun!

## TODOne
1) https://gitlab.com/jksimoniii/wordfly/-/commit/624a1cfb25a4fc6f6a68aa73e558c88e8efac155 I implemented the index using
`django_elasticsearch_dsl.Document` and excluded a number of fields in this index from `api.models.Lesson` because they 
weren't needed to meet the search requirements. The index uses a `results` property instead of data in an attempt to be 
more explicit about what data the document consists of. I considered separating all "elasticsearch stuff" into another app, but 
I didn't want to get lost in that decision for this exercise. I also updated `bin/entrypoint.sh` to rebuild the search    
index. Although this is not ideal for large datasets, it was a quick way to ensure the next developer doesn't forget to 
run `manage.py search_index --rebuild`. I'm sure there's a better solution for this :shrug:

2) Provided automagically in 1

3) https://gitlab.com/jksimoniii/wordfly/-/commit/fc354ccfb95e30bf8b4feaebcd700048e866ff36 provides the serializers/viewset/url
to expose these APIs. I structured route in a way that may make future-searchable more predictable `/api/v1/search/:somemodel`. 
This is the point in time I almost turned around and separated "elasticsearch stuff". Anyhow, tucking these classes away in 
`api.views.search` makes it feel decoupled enough from the standard viewsets. I failed to provide the implementation of these 2 
use cases in the allotted 4 hours. 
  - (Search) All lessons where a given word was failed / passed
  - (Filter) All lessons where at least X words had at least 1 attempt
  
 I may come back to them at some point before you review this. I found I lost a some of my time digging through 
 `django-elasticsearch-dsl-drf` documentation (which I was not super impressed with). I eventually found myself with a debugger
 stepping through their library to see how the implementation worked.
 
#### Did you run into any problems?
Yes. I've added what I could to Gitlab Issues on my fork. Some really aren't issues, but things that I've witness cause 
confusion when trying to scale a team (missing docker networks, `python3` vs `python`).

#### Did you have to make any assumptions?
Sorta. At first, I wanted to try to modify the existing `LessonView.list` action to serve as the exposed searchable endpoint.
But, I had second thoughts and assumed that maybe this view was already used in the application. I pivoted to the `api.views.search`
submodule at this point. I lost a little time here trying to make that work (overriding `get_queryset` was making it feel wrong)  

#### If you were asked to continue working on this, what would be your next steps?
(Assuming I finished all the requirements) ... I would want to test object permissions. I'm pretty sure I'm exposing all 
`LessonDocuments` regardless of your user. That's probably not good :)

#### How would you deliver information about this new endpoint & its functionality to the frontend team?
I've used both Postman and Swagger to help document APIs. I've included exports of Postman collection/environment that you 
can import and play with.

[Postman Collection](docs/wordfly.postman_collection.json)
[Postman Environment](docs/wordfly.postman_environment.json)
