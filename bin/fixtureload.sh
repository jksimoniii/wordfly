#!/bin/ash
for file in api/fixtures/test*.yaml; do
    echo "Loading $file"
    python3 manage.py loaddata --app api $(basename $file)
done
echo "All fixtures loaded"
