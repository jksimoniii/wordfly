#!/bin/ash

APP="core.wsgi:application"
TIMEOUT="60"
WORKERS="4"
KEEP_ALIVE="900"

start_gunicorn() {
    if [ -n "$PORT" ]; then
        BIND_PT="0.0.0.0:"$PORT
        LOG_LEVEL="info"
        # Production settings
        gunicorn -b $BIND_PT $APP -c gunicorn_config.py --timeout=$TIMEOUT --graceful-timeout=30 --workers=$WORKERS --keep-alive $KEEP_ALIVE --log-level=$LOG_LEVEL --worker-class=$1

    else
        BIND_PT="0.0.0.0:5000"
        LOG_LEVEL="debug"
        # Development settings. Besides the log level, the only difference here is that we're passing the --reload flag 
        gunicorn -b $BIND_PT $APP -c gunicorn_config.py --reload --timeout=$TIMEOUT --graceful-timeout=30 --workers=$WORKERS --keep-alive $KEEP_ALIVE --log-level=$LOG_LEVEL --worker-class=$1
    fi
}

if [ "$MIGRATE" == 1 ]; then
    python3 manage.py makemigrations --noinput;
    if python3 manage.py migrate; then
        ash bin/fixtureload.sh
        python3 manage.py search_index --rebuild -f
        start_gunicorn sync
    fi
else
    start_gunicorn gevent
fi
