FROM alpine:edge

RUN apk --no-cache --update upgrade
RUN apk --no-cache --update add \
    python3 \
    python3-dev \
    py3-pip \
    py3-psycopg2 \
    py3-gevent \ 
    postgresql-client \
    curl \
    libmagic

RUN pip3 install --upgrade pip setuptools \
    && rm -rf /root/.cache/pip

ADD requirements.txt /opt/sdx/requirements.txt

RUN pip3 install -r /opt/sdx/requirements.txt \
    && rm -rf /root/.cache/pip

RUN adduser -D web
USER root

ADD . /opt/sdx
WORKDIR /opt
RUN /usr/bin/find . -type d -exec chown web {} \;
WORKDIR /opt/sdx
RUN python3 -m compileall -f -q .
USER web
RUN python3 manage.py collectstatic --noinput
CMD /opt/sdx/bin/entrypoint.sh