from django.contrib import admin
from .models import (
    Lesson,
    User,
    Wordlist
)

admin.site.register(Lesson)
admin.site.register(User)
admin.site.register(Wordlist)
