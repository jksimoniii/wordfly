from rest_framework.test import APIClient
from django.test import TestCase
from api.models import Wordlist, User


class WordlistReadTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(
            email='test@test.com', password='test')
        cls.wordlist = Wordlist.objects.create(
            owner=cls.user, words=['word1', 'word2', 'word3'], name='TestWordlist')
        cls.client = APIClient()

    def testUnauthenticatedMayList(self):
        response = self.client.get('/api/v1/wordlists/')
        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.json()['count'], 1)
        self.assertEqual(response.json()['next'], None)
        self.assertEqual(response.json()['previous'], None)
        self.assertEqual(response.json()['results'][0]['id'], self.wordlist.id)
        self.assertEqual(
            response.json()['results'][0]['owner'], self.user.id)
        self.assertIn('created', response.json()['results'][0].keys())
        self.assertIn('modified', response.json()['results'][0].keys())
        self.assertEqual(
            response.json()['results'][0]['name'], self.wordlist.name)
        self.assertEqual(
            response.json()['results'][0]['words'], self.wordlist.words)

    def testUnauthenticatedMayRetrieve(self):
        response = self.client.get(
            '/api/v1/wordlists/{}/'.format(self.wordlist.id))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['id'], self.wordlist.id)
