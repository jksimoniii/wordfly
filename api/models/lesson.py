from django.contrib.postgres.fields import JSONField
from django.db import models
from .base import BaseModel


class Lesson(BaseModel):
    # NOTE: `data` is a JSONField, not a ForeignKey to a Wordlist object.
    # this way we can be sure edits to Wordlists don't corrupt recorded lessons
    data = JSONField(
        default=dict, help_text="An object containing words keyed to attempt counts")

    def __str__(self):
        return "({} Owner: {} Tags: {})".format(
            self.created,
            self.owner.email,
            # ', '.join([val for val in self.tags.values_list('label', flat=True)])
            ', '.join([])
        )
