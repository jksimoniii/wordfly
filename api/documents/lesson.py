from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry

from api.models import Lesson


@registry.register_document
class LessonDocument(Document):
    """
    Searchable representation of api.models.Lesson, designed to fulfill the following
    use cases:
      - (Search) All lessons associated with a wordlist with a given name
      - (Search) All lessons that contain a given word
      - (Search) All lessons where a given word was failed / passed
      - (Filter) All lessons where at least X words had at least 1 attempt
      - (Filter) All lessons before/after a certain date

    Using django_elasticsearch_dsl.Document connects signals receivers to
    api.models.Lesson.
    """
    wordlist = fields.TextField()  # TODO: might have to add FK to Lesson
    results = fields.NestedField()

    class Index:
        name = 'lessons'

    class Django:
        model = Lesson
        fields = (
            'created',
        )

    def prepare_results(self, instance):
        return [
            {
                'word': word,
                'attempts': data.get('attempts'),
                'passed': data.get('nailedIt'),
             } for word, data in instance.data.items()
        ]

    def prepare_wordlist(self, instance):
        # TODO: Where do we get this data if FK is missing on Lesson?
        pass
