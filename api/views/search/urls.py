"""
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
"""
from django.urls import path, include
from rest_framework import routers
from api.views.search import LessonDocumentViewSet

router = routers.SimpleRouter()
router.register(r'lessons', LessonDocumentViewSet, basename='search_lessons')

urlpatterns = [
    path('', include(router.urls)),
]
