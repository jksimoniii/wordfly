from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
    DefaultOrderingFilterBackend,
    SearchFilterBackend,
    CompoundSearchFilterBackend,
)
from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet

from api.documents import LessonDocument
from api.views.search.serializers import LessonDocumentSerializer


class LessonDocumentViewSet(BaseDocumentViewSet):
    """
    The LessonDocument View
    """
    document = LessonDocument
    serializer_class = LessonDocumentSerializer
    filter_backends = [
        FilteringFilterBackend,
        OrderingFilterBackend,
        DefaultOrderingFilterBackend,
        CompoundSearchFilterBackend,
    ]
    search_fields = ('wordlist',)
    search_nested_fields = {
        'words': {
            'path': 'results',
            'fields': ['word']
        }
    }
    filter_fields = {
        'date': 'created'
    }
    ordering_fields = {
        'date': 'created'
    }
    ordering = ('created',)
