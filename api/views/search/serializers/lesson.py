from django_elasticsearch_dsl_drf import serializers

from api.documents import LessonDocument


class LessonDocumentSerializer(serializers.DocumentSerializer):
    """
    Serializer for the api.documents.LessonDocument
    """
    class Meta:
        document = LessonDocument
