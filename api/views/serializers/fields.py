from rest_framework import serializers
import jsonschema


class JSONSchemaField(serializers.Field):
    schema = {}

    def to_representation(self, obj):
        return obj

    def to_internal_value(self, data):  # pragma: no cover
        try:
            jsonschema.validate(data, self.schema)
        except jsonschema.exceptions.ValidationError as e:
            raise serializers.ValidationError(e.message)
        return data


class WordlistDataField(JSONSchemaField):
    '''
    Wordlist Data should be a simple array of words.
    Ex: ['word1', 'word2']
    '''
    schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "WordlistData",
        "definitions": {
            "words": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        },
        "$ref": "#/definitions/words"
    }


class LessonDataField(JSONSchemaField):
    '''
    Lesson Data should be an object containing words keyed to the number
    of attempts made on those words.

    Ex: {'word1': 1, 'word2': 4}
    '''
    schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "LessonData",
        "definitions": {
            "data": {
                "type": "object"
            }
        },
        "$ref": "#/definitions/data"
    }
