from rest_framework import viewsets, mixins
from rest_framework.response import Response
from api.models import Lesson
from .serializers import LessonDataSerializer, LessonSerializer


class LessonViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet
):
    '''
    Viewset to provide authenticated access to list, 
    retrieve, create and update Lesson objects.
    '''
    model = Lesson
    serializer_class = LessonSerializer

    def get_queryset(self):
        if self.request.user.is_staff:
            return self.model.objects.all()
        return self.model.objects.filter(
            owner=self.request.user
        )

    def get_serializer_class(self):
        if self.action in {
            'update',
            'partial_update',
            'create'
        }:
            return LessonDataSerializer
        return LessonSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer_class()(data=request.data)
        serializer.is_valid(raise_exception=True)

        lesson = Lesson.objects.create(
            owner=request.user,
            data=serializer.data.get('data', dict())
        )

        resp_data = LessonSerializer(lesson).data
        return Response(resp_data, status=201)

    def update(self, request, pk, *args, **kwargs):
        '''
        Explicit update/partial_update view because I don't like
        model serializers, and because I'm not renaming my fields 
        just to make DRF's undocumented assumptions work right
        '''
        lesson = self.get_object()
        serializer = self.get_serializer_class()(data=request.data)
        serializer.is_valid(raise_exception=True)

        # explicit check for None since we should allow updating to empties
        if serializer.data.get('data', None) is not None:
            lesson.data = serializer.data.get('data')
            lesson.save()

        if isinstance(serializer.data.get('tags'), list):
            lesson.tags.set(serializer.data.get('tags'))

        lesson.refresh_from_db()
        resp_data = LessonSerializer(lesson).data
        return Response(resp_data, status=200)
