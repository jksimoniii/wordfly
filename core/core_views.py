from django.http import HttpResponse
from django.views import View


class HealthCheck(View):  # pragma: no cover
    def get(self, request):
        return HttpResponse('ok! :)')
